do_configure:append() {
	if [ ! -z "${LINUX_VERSION_EXTENSION}" ]; then
		echo "# Global settings from linux recipe" >> ${B}/.config
		echo "CONFIG_LOCALVERSION="\"${LINUX_VERSION_EXTENSION}\" >> ${B}/.config
		yes '' | oe_runmake -C ${S} O=${B} CC="${KERNEL_CC}" LD="${KERNEL_LD}" oldconfig
	fi
}
