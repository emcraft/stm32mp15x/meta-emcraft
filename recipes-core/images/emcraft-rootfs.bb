# Copyrigh (C) 2021 Emcraft Systems

require recipes-st/images/st-image-weston.bb

MACHINE_EXTRA_RDEPENDS += "kernel-module-rpmsg-tty"

CORE_IMAGE_EXTRA_INSTALL += " linux-firmware murata-binaries "
IMAGE_ROOTFS_MAXSIZE = "1200000"
